package sg.edu.nus.comp.cs4218.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class Utils {
		
	private Utils() {};
   
	private static final int BUFFER_SIZE = 8192;

    public static void copy(InputStream is, OutputStream os) throws IOException {
    	int b;
        while (-1 != (b = is.read())) {
        	os.write(b);
        }
    }
    
    //TODO test this method with multiple threads
    public static Pair<OutputStream, InputStream> createPipe() throws IOException {
    	PipedOutputStream output = new PipedOutputStream();
        PipedInputStream input = new PipedInputStream(output);
    	return new Pair<OutputStream, InputStream>(output, input);
    }
    
    public static InputStream convertStringToInputStream(String s) {
    	InputStream is = new ByteArrayInputStream(s.getBytes());
		return is;
    }

}
