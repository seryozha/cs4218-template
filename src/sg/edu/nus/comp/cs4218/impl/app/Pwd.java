package sg.edu.nus.comp.cs4218.impl.app;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.PwdException;
import sg.edu.nus.comp.cs4218.impl.Utils;

public class Pwd implements Application {

	public void run(String[] args, InputStream stdin, OutputStream stdout)
			throws AbstractApplicationException {
		
		InputStream is = Utils.convertStringToInputStream(Environment.currentDirectory + "\n");
		try {
			Utils.copy(is, stdout);
		} catch (IOException e) {
			throw new PwdException("IO error");
		}
		
	}

}
