package sg.edu.nus.comp.cs4218.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.Command;
import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.Shell;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;
import sg.edu.nus.comp.cs4218.impl.app.Cat;
import sg.edu.nus.comp.cs4218.impl.app.Cd;
import sg.edu.nus.comp.cs4218.impl.app.Ls;
import sg.edu.nus.comp.cs4218.impl.app.Pwd;
import sg.edu.nus.comp.cs4218.impl.cmd.Call;

public class BasicShell implements Shell {

	public void parseAndEvaluate(String cmdline, OutputStream stdout) throws AbstractApplicationException, ShellException {
		Command cmd = parse(cmdline);
		cmd.evaluate(null, stdout);
	}

	public String prompt() {
		int sep = Environment.currentDirectory.lastIndexOf(File.separator);
		String dir;
		if (sep >= 0) {
			dir = Environment.currentDirectory.substring(sep + File.separator.length());
		} else {
			dir = Environment.currentDirectory;
		}
		return "[" + dir + "] $ ";
	}
	
	private Command parse(String cmdline) throws ShellException {
		String[] args = cmdline.split("\\s");
		if (args.length < 1) {
			throw new ShellException("wrong command line");
		}
		String appName = args[0];
		String[] appArgs = Arrays.copyOfRange(args, 1, args.length);
		Application app;
		
		if (appName.equals("cat")) {
			app = new Cat();
		} else if (appName.equals("cd")) {
			app = new Cd();
		} else if (appName.equals("ls")) {
			app = new Ls();
		} else if (appName.equals("pwd")) {
			app = new Pwd();
		} else {
			throw new ShellException("application not found");
		}
		
		Command cmd = new Call(app, appArgs);
		return cmd;
	}
}
