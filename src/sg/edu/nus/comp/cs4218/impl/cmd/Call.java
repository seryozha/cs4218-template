package sg.edu.nus.comp.cs4218.impl.cmd;

import java.io.InputStream;
import java.io.OutputStream;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.Command;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;

public class Call implements Command {
	
	private Thread thread;
	private volatile AbstractApplicationException exception;
	private Application app;
	private String[] args;
	
	public Call(Application app, String[] args) {
		this.app = app;
		this.args = args;
	}
	
	public void evaluate(final InputStream stdin, final OutputStream stdout)
			throws AbstractApplicationException, ShellException {
		if (thread == null) {
			thread = new Thread(new Runnable() {
				public void run () {
					try {
						app.run(args, stdin, stdout);
					} catch (AbstractApplicationException e) {
						exception = e; 
					}
				}	
			});
			thread.start();
			try {
				thread.join();
				if (exception != null) {
					throw exception;
				}
			} catch (InterruptedException e) {
				throw new ShellException("error while execution application");
			}
		}
	}

	public void terminate() {
		if (thread != null) {
			thread.stop();
			thread = null;
		}
	}

}
