package sg.edu.nus.comp.cs4218.impl;

import java.util.Scanner;

import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.ShellException;

public class Terminal {
	
	public static void main(String[] args) {
		BasicShell shell = new BasicShell();
		Scanner userInput = new Scanner(System.in);
		while (true) {
			System.out.print(shell.prompt());
			String cmdline = userInput.nextLine();
			try {
				shell.parseAndEvaluate(cmdline, System.out);
			} catch (AbstractApplicationException e) {
				System.out.println(e.getMessage());
			} catch (ShellException e) {
				System.out.println(e.getMessage());
			}
		}
	}

}
