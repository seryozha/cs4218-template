package sg.edu.nus.comp.cs4218.impl.app;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.CatException;
import sg.edu.nus.comp.cs4218.impl.Utils;

public class Cat implements Application {

	public void run(String[] args, InputStream stdin, OutputStream stdout)
			throws AbstractApplicationException {
		for (String filename: args) {
			File f = new File(Environment.currentDirectory, filename);
			InputStream is;
			try {
				is = new BufferedInputStream(new FileInputStream(f));
				Utils.copy(is, stdout);
				is.close();
				stdout.write('\n');
			} catch (FileNotFoundException e) {
				throw new CatException("file " + filename + " not found");
				
			} catch (IOException e) {
				throw new CatException("IO error");
			}
		}
		
	}

}
