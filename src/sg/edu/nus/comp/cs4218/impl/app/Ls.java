package sg.edu.nus.comp.cs4218.impl.app;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.LsException;
import sg.edu.nus.comp.cs4218.impl.Utils;

public class Ls implements Application {

	public void run(String[] args, InputStream stdin, OutputStream stdout)
			throws AbstractApplicationException {
		File dir;
		String outputString = "";
		if (args.length == 0) {
			dir = new File(Environment.currentDirectory);
		} else 	if (args.length == 1) {
			String dirname = args[0];
			dir = new File(Environment.currentDirectory, dirname);
			if (! (dir.exists() && dir.isDirectory()) ) {
				throw new LsException("directory " + dirname + " not found");
			}
		} else {
			throw new LsException("too many arguments");
		}
		
		File[] listOfFiles = dir.listFiles(); 
 		for (File f: listOfFiles) 
		{
 			if (outputString.equals("")) {
 				outputString = f.getName(); 				
 			} else {
 				outputString = outputString + "\t" + f.getName();
 			}
		}
 		
 		InputStream is = Utils.convertStringToInputStream(outputString + "\n");
		try {
			Utils.copy(is, stdout);
		} catch (IOException e) {
			throw new LsException("IO error");
		}
	}
}
