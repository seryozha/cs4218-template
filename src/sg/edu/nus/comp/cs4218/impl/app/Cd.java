package sg.edu.nus.comp.cs4218.impl.app;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import sg.edu.nus.comp.cs4218.Application;
import sg.edu.nus.comp.cs4218.Environment;
import sg.edu.nus.comp.cs4218.exception.AbstractApplicationException;
import sg.edu.nus.comp.cs4218.exception.CdException;

public class Cd implements Application {

	public void run(String[] args, InputStream stdin, OutputStream stdout)
			throws AbstractApplicationException {
		if (args.length != 1) {
			throw new CdException("not enough/too many arguments");
		}
		String dirname = args[0];
		File f = new File(Environment.currentDirectory, dirname);
		if (! (f.exists() && f.isDirectory()) ) {
			throw new CdException("directory " + dirname + " not found");
		}
		try {
			Environment.currentDirectory = f.getCanonicalPath();
		} catch (IOException e) {
			new CdException("IO error");
		}		
	}

}